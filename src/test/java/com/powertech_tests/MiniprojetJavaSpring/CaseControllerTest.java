package com.powertech_tests.MiniprojetJavaSpring;

import static org.mockito.Mockito.when;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.Date;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.powertech_tests.MiniprojetJavaSpring.domain.Case;
import com.powertech_tests.MiniprojetJavaSpring.service.CaseService;
import com.powertech_tests.MiniprojetJavaSpring.web.CaseController;

@WebMvcTest(controllers = CaseController.class)
@ExtendWith(MockitoExtension.class)
public class CaseControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	private CaseService caseService;

	@InjectMocks
	private CaseController caseController;

	@BeforeEach
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(caseController).build();
	}

	@Test
	public void testGetCase() throws Exception {
		Long caseId = 1L;
		Case testCase = new Case();
		testCase.setCaseId(caseId);
		testCase.setTitle("Test Title");
		testCase.setDescription("Test Description");
		testCase.setCreationDate(new Date());
		testCase.setLastUpdateDate(new Date());

		when(caseService.findById(caseId)).thenReturn((ResponseEntity) ResponseEntity.ok(testCase));

		mockMvc.perform(get("/cases/{caseId}", caseId)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.caseId").value(caseId)).andExpect(jsonPath("$.title").value("Test Title"))
				.andExpect(jsonPath("$.description").value("Test Description"));
	}

	@Test
	public void testGetCaseNotFound() throws Exception {
		Long caseId = 1L;

		when(caseService.findById(caseId)).thenReturn(ResponseEntity.notFound().build());

		mockMvc.perform(get("/cases/{caseId}", caseId)).andExpect(status().isNotFound());
	}

	@Test
	public void testUpdateCase() throws Exception {
		Long caseId = 1L;
		Case updatedCase = new Case();
		updatedCase.setTitle("Updated Title");
		updatedCase.setDescription("Updated Description");

		when(caseService.updateCase(caseId, updatedCase)).thenReturn((ResponseEntity) ResponseEntity.ok(updatedCase));

		mockMvc.perform(put("/cases/{caseId}", caseId).contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(updatedCase))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.title").value("Updated Title"))
				.andExpect(jsonPath("$.description").value("Updated Description"));
	}

	@Test
	public void testCreateCase() throws Exception {
		Case newCase = new Case();
		newCase.setTitle("New Title");
		newCase.setDescription("New Description");

		when(caseService.createCase(newCase)).thenReturn((ResponseEntity) ResponseEntity.ok(newCase));

		mockMvc.perform(post("/cases").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(newCase))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.title").value("New Title"))
				.andExpect(jsonPath("$.description").value("New Description"));
	}

	@Test
	public void testDeleteCase() throws Exception {
		Long caseId = 1L;

		when(caseService.deleteCase(caseId)).thenReturn(ResponseEntity.ok().build());

		mockMvc.perform(delete("/cases/{caseId}", caseId)).andExpect(status().isOk());
	}

	@Test
	public void testDeleteCaseNotFound() throws Exception {
		Long caseId = 1L;

		when(caseService.deleteCase(caseId)).thenReturn(ResponseEntity.notFound().build());

		mockMvc.perform(delete("/cases/{caseId}", caseId)).andExpect(status().isNotFound());
	}

}
