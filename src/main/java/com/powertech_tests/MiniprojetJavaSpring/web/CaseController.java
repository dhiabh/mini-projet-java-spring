package com.powertech_tests.MiniprojetJavaSpring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.powertech_tests.MiniprojetJavaSpring.domain.Case;
import com.powertech_tests.MiniprojetJavaSpring.service.CaseService;

@RestController
@RequestMapping("/cases")
public class CaseController {

	@Autowired
	private CaseService caseService;

	@GetMapping("{caseId}")
	public ResponseEntity<?> getCase(@PathVariable Long caseId) {
		return caseService.findById(caseId);
	}

	@PutMapping("{caseId}")
	public ResponseEntity<?> updateCase(@PathVariable Long caseId, @RequestBody Case updatedCase) {
		return caseService.updateCase(caseId, updatedCase);
	}

	@PostMapping
	public ResponseEntity<?> createCase(@RequestBody Case newCase) {
		return caseService.createCase(newCase);
	}

	@DeleteMapping("{caseId}")
	public ResponseEntity<?> deleteCaseById(@PathVariable Long caseId) {
		return caseService.deleteCase(caseId);
	}
}
