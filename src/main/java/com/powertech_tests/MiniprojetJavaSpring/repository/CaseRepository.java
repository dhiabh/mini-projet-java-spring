package com.powertech_tests.MiniprojetJavaSpring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.powertech_tests.MiniprojetJavaSpring.domain.Case;

public interface CaseRepository extends JpaRepository<Case, Long> {

}
