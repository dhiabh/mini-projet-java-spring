package com.powertech_tests.MiniprojetJavaSpring.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.powertech_tests.MiniprojetJavaSpring.domain.Case;
import com.powertech_tests.MiniprojetJavaSpring.repository.CaseRepository;

@Service
public class CaseService {

	@Autowired
	private CaseRepository caseRepository;

	public ResponseEntity<?> findById(Long caseId) {
		Optional<Case> caseOpt = caseRepository.findById(caseId);
		return caseOpt.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	public ResponseEntity<?> updateCase(Long caseId, Case updatedCase) {
		if (areFieldsValid(updatedCase)) {

			Optional<Case> existingCaseOpt = caseRepository.findById(caseId);

			if (existingCaseOpt.isPresent()) {
				Case existingCase = existingCaseOpt.get();
				existingCase.setTitle(updatedCase.getTitle());
				existingCase.setDescription(updatedCase.getDescription());
				existingCase.setLastUpdateDate(new Date()); // Update the lastUpdateDate

				caseRepository.save(existingCase);

				return ResponseEntity.ok(existingCase);
			} else {
				return ResponseEntity.notFound().build();
			}
		} else
			return ResponseEntity.badRequest().build();
	}

	public ResponseEntity<?> createCase(Case newCase) {
		if (areFieldsValid(newCase)) {

			Case caseToCreate = new Case();
			caseToCreate.setCreationDate(new Date());
			caseToCreate.setLastUpdateDate(new Date());
			caseToCreate.setTitle(newCase.getTitle());
			caseToCreate.setDescription(newCase.getDescription());

			caseRepository.save(caseToCreate);

			return ResponseEntity.ok(caseToCreate);
		} else
			return ResponseEntity.badRequest().build();

	}

	public ResponseEntity<?> deleteCase(Long caseId) {
		Optional<Case> existingCaseOpt = caseRepository.findById(caseId);

		if (existingCaseOpt.isPresent()) {
			caseRepository.deleteById(caseId);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	public Boolean areFieldsValid(Case caseToValidate) {
		return caseToValidate.getTitle() != null && caseToValidate.getDescription() != null;
	}

}
